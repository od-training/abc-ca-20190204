import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Video } from 'src/app/common/interfaces';
import { AppState, getVideos } from 'src/app/state';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  videos: Observable<Video[]>;

  constructor(store: Store<AppState>, ar: ActivatedRoute, router: Router) {
    const id$: Observable<string> = ar.queryParams.pipe(
      map(params => params.selectedVideoId)
    );
    this.videos = combineLatest(store.select(getVideos), id$).pipe(
      tap(([videos, id]) => {
        // use tap because this is a side effect
        if (!id) {
          router.navigate(
            [], { queryParams: { selectedVideoId: videos[0].id } }
          );
        }
      }),
      // use an underscore on the name to let the linter know that
      // it's ok not to use the id value
      map(([videos, _id]) => videos)
    );
  }
}
