import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { Video } from './common/interfaces';

const api = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  constructor(private http: HttpClient) {
  }

  load() {
    return this.http.get<Video[]>(api + '/videos').pipe(
      map(videos => {
        return videos.filter(video => video.title.startsWith('Angular'));
      })
    );
  }

  getVideo(id: string) {
    return this.http.get<Video>(api + '/videos/' + id);
  }
}
