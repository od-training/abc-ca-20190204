# Angular Boot Camp
[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager (target application)](http://videomanager.angularbootcamp.com)

[Workshop Repo](https://bitbucket.org/od-training/abc-ca-20190204)

[Sample Videos Data](https://api.angularbootcamp.com/videos)

[Survey](http://angularbootcamp.com/survey)

# Resources
[TypeScript Playground](http://www.typescriptlang.org/play/)

[DOM Events](https://developer.mozilla.org/en-US/docs/Web/Events)

[Exploring ES6](http://exploringjs.com/es6/index.html)

[Angular Update](https://update.angular.io/)

[Component Interaction](https://angular.io/guide/component-interaction)

[Upgrading AngularJS](https://www.upgradingangularjs.com/)

[Managing State in Angular (video)](https://www.youtube.com/watch?v=eBLTz8QRg4Q)

## VS Code extensions
[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)

[Angular File
Changer](https://marketplace.visualstudio.com/items?itemName=john-crowson.angular-file-changer)

[Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

# Books
[Build Reactive Websites with
RxJS](https://pragprog.com/book/rkrxjs/build-reactive-websites-with-rxjs)

[Functional Light JavaScript](https://github.com/getify/Functional-Light-JS)

## RXJS Pipeable Operators Resources
  [RXMarbles](http://rxmarbles.com)
  [LearnRXJS](https://www.learnrxjs.io/)
  [Seven Operators to Get Started with RxJS (Article)](https://www.infoq.com/articles/rxjs-get-started-operators)
  ["I Switched a Map" video](https://www.youtube.com/watch?v=rUZ9CjcaCEw)
  [Reactive Visualizations](https://reactive.how/)
  [Operator Decision Tree](https://rxjs-dev.firebaseapp.com/operator-decision-tree)